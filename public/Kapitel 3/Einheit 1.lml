<lml>
  <lesson>
    <h1>Das Arduino-Board</h1>
    <h3>Die Pins</h3>
    <p>Ein Arduino-Board hat mehrere Anschlüsse, die Pins genannt werden. Diese Pins haben unterschiedliche Eigenschaften und lassen sich in verschiedene Kategorien unterteilen.</p>
    <h3>Digitale Pins</h3>
    <p>Nahezu alle Pins eines Arduino-Boards lassen sich für digitalen Input oder Output verwenden. Digital heißt, dass diese Pins nur zwei Zustände einnehmen können, HIGH oder LOW. HIGH steht für die Betriebsspannung des Boards, LOW bedeutet 0V (GND-Pin des Boards) Spannung.</p>
    <h3>Analoge Pins</h3>
    <p>Analoge Pins, gekennzeichnet durch ein <m>A</m> vor der Nummer, können unterschiedliche Spannungen messen. In der Regel lassen sich 1024 (0 bis 1023) verschiedene Spannungsniveaus zwischen 0V und der Betriebsspannung messen. Das heißt, das zum Beispiel der Wert 512 die Hälfte der Betriebsspannung repräsentiert.</p>
    <h3>PWM-fähige Pins</h3>
    <p>Einige Pins unterstützen Pulsweitenmodulation, kurz PWM. Bei PWM wird die Spannung in kurzen Intervallen ein- und ausgeschaltet, so dass es für ein Messgerät aussieht, als würden unterschiedliche Spannungen erzeugt werden. An diesen Pins lassen sich also theoretisch Spannungen von 0V bis zur Betriebsspannung anlegen. Dabei können Werte zwischen 0 und 255 verwendet werden. 127 wäre also hier zum Beispiel die halbe Betriebsspannung. In Vino sind diese Pins übrigens mit einem <m>~</m> gekennzeichnet.</p>
    <h3>Spezielle Pins</h3>
    <p>Manche Pins erfüllen nur spezielle Funktionen, wie zum Beispiel die Spannungsversorgung durch den 5V-Pin. Diese Pins lassen sich meist gar nicht über die Programmierung ansteuern. Bei Vino sind sie deswegen nicht explizit dargestellt.</p>
  </lesson>
  <quiz>
    <question>
      <h2>Wie viele Pins eines Arduino-Boards unterstützten digitalen Input und Output?</h2>
    </question>
    <box>
      <option>Genau ein Pin</option>
      <option correct="true">Die meisten Pins</option>
      <option>Nur wenige Pins</option>
    </box>
  </quiz>
  <quiz>
    <question>
      <h2>Welche der folgenden Aussgagen sind korrekt?</h2>
    </question>
    <box>
      <option correct="true">Ein gemessener Wert von 1023 an einem analogen Pin heißt, dass Betriebsspannung anliegt.</option>
      <option>Die Stärke der PWM kann in Zahlen von 0 bis 1023 angegeben werden.</option>
      <option correct="true">PWM wird fast ausschließlich zur Spannungsversorgung verwendet.</option>
      <option>Analoge Pins können auch analoge Werte als Spannung ausgeben.</option>
    </box>
  </quiz>
</lml>
